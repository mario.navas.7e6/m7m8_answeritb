package com.example.trivialtest.model

import android.media.Image

data class Question(
    val question: String,
    val answerFormat: String,
    val theme: String,
    val answerCorrect :String,
    val options: List<String>,
    val image: Int

)