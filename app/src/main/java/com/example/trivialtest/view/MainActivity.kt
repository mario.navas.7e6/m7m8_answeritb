package com.example.trivialtest.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.trivialtest.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(supportActionBar != null)
            supportActionBar?.hide()
        setContentView(R.layout.activity_main)
    }
}