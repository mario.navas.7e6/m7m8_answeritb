package com.example.trivialtest.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.OnClickListener
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.trivialtest.R
import com.example.trivialtest.databinding.FragmentQuestionsBinding
import com.example.trivialtest.model.Question
import com.example.trivialtest.viewmodel.QuestionsViewModel

 class QuestionsFragment : Fragment(), OnClickListener {

    lateinit var binding: FragmentQuestionsBinding
    private lateinit var question: Question
    private lateinit var countDownTimer: CountDownTimer
    private val viewModel: QuestionsViewModel by activityViewModels()
    private var score = 0
    private var userAnswer = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentQuestionsBinding.inflate(layoutInflater)
        binding.answer1.setOnClickListener(this)
        binding.answer2.setOnClickListener(this)
        binding.answer3.setOnClickListener(this)
        binding.answer4.setOnClickListener(this)
        binding.trues.setOnClickListener(this)
        binding.falses.setOnClickListener(this)
        binding.answerTextButton.setOnClickListener(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        countDownTimer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.myProgress.progress = (millisUntilFinished / 1000).toInt()
            }

            override fun onFinish() {
                userAnswer = ""
                answerCheck(0)
            }
        }

        viewModel.questionIndex = 0

        viewModel.shuffleQuestions()

        viewModel.currentQuestion.observe(viewLifecycleOwner, Observer { currentQuestion ->
            question=currentQuestion
            updateQuestion(question)


        })


        binding.nextQuestion.setOnClickListener {

            viewModel.submitScore(score)

            if (viewModel.isLastQuestion()) {
                findNavController().navigate(R.id.action_questionsFragment_to_resultFragment)
                viewModel.getNextQuestion()
            } else {
                viewModel.getNextQuestion()
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private fun updateQuestion(question: Question) {

        score = 0
        binding.nextQuestion.visibility = INVISIBLE
        binding.answerPoints.visibility = INVISIBLE
        binding.answerCorrectOrNot.visibility = INVISIBLE
        binding.questionText.text = question.question
        binding.imageView.setImageResource(question.image)

        when(question.answerFormat){
            "four" ->{
                binding.answer1.text = question.options[0]
                binding.answer2.text = question.options[1]
                binding.answer3.text = question.options[2]
                binding.answer4.text = question.options[3]

                binding.answer1.visibility = VISIBLE
                binding.answer2.visibility = VISIBLE
                binding.answer3.visibility = VISIBLE
                binding.answer4.visibility = VISIBLE
                binding.trues.visibility = INVISIBLE
                binding.falses.visibility = INVISIBLE
                binding.answerEditText.visibility = INVISIBLE
                binding.answerTextButton.visibility =   INVISIBLE



            }
            "trueFalse"->{
                binding.trues.text = "true"
                binding.falses.text = "false"

                binding.answer1.visibility = INVISIBLE
                binding.answer2.visibility = INVISIBLE
                binding.answer3.visibility = INVISIBLE
                binding.answer4.visibility = INVISIBLE
                binding.trues.visibility = VISIBLE
                binding.falses.visibility = VISIBLE
                binding.answerEditText.visibility = INVISIBLE
                binding.answerTextButton.visibility = INVISIBLE

            }
            "input"->{
                binding.answer1.visibility = INVISIBLE
                binding.answer2.visibility = INVISIBLE
                binding.answer3.visibility = INVISIBLE
                binding.answer4.visibility = INVISIBLE
                binding.trues.visibility = INVISIBLE
                binding.falses.visibility = INVISIBLE
                binding.answerEditText.visibility = VISIBLE
                binding.answerEditText.text = null
                binding.answerTextButton.visibility = VISIBLE


            }
        }

        val backgroundColor = when(question.theme){
            "arts"-> R.color.pink_400
            "history"->R.color.sea_green
            "science"->R.color.royal_blue
            "sports"->R.color.orange_400
            "geography"->R.color.purple_400
            else -> R.color.red_100
        }
        binding.root.background = ContextCompat.getDrawable(requireContext(),backgroundColor)

        countDownTimer.start()

    }

    override fun onClick(v: View?) {
        val time = System.currentTimeMillis()
        countDownTimer.cancel()

        val button=v as Button
        userAnswer = if(button.toString() == "submit"){
                        binding.answerEditText.toString()
                    }else{
                        button.toString()

                    }

        answerCheck(time)

    }


     @SuppressLint("SetTextI18n", "ResourceAsColor")
     fun answerCheck(time: Long){

         binding.answer1.visibility = INVISIBLE
         binding.answer2.visibility = INVISIBLE
         binding.answer3.visibility = INVISIBLE
         binding.answer4.visibility = INVISIBLE
         binding.trues.visibility = INVISIBLE
         binding.falses.visibility = INVISIBLE
         binding.answerEditText.visibility = INVISIBLE
         binding.answerTextButton.visibility = INVISIBLE
         binding.nextQuestion.visibility = VISIBLE
         binding.answerPoints.visibility = VISIBLE
         binding.answerCorrectOrNot.visibility = INVISIBLE

         if(userAnswer==question.answerCorrect){

             score += when (time){
                 in 9000..10000 -> 100
                 in 8000..9000 -> 80
                 in 7000..7999 -> 70
                 in 6000 .. 6999 -> 60
                 in 5000 .. 5999 -> 50
                 in 4000 .. 4999 -> 40
                 in 3000 .. 3999 -> 30
                 in 1 .. 2999 -> 20
                 else -> 0
             }

             binding.answerCorrectOrNot.text = "Correct Answer!"
             binding.answerCorrectOrNot.setTextColor(R.color.light_green)

         }else{
             score = 0
             binding.answerCorrectOrNot.text = "The Correct Answer Was : ${question.answerCorrect}"
             binding.answerCorrectOrNot.setTextColor(R.color.red_600)
         }
         binding.answerPoints.text = "+$score"
     }
}