package com.example.trivialtest.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.trivialtest.R
import com.example.trivialtest.databinding.FragmentRankingBinding
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

class RankingFragment : Fragment() {

    private lateinit var binding: FragmentRankingBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRankingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val rankingFile = File(requireContext().filesDir, "ranking.txt")
//        if (rankingFile.exists()) {
//            val rankingLines = rankingFile.readLines()
//            for (line in rankingLines) {
////                binding.rankingText.append(line)
////                binding.rankingText.append("\n")
//            }
//        }
    }

    fun readFile(){
        var player = mutableListOf<MutableList<String>>()
        var lines=0
        val file = InputStreamReader(requireActivity().openFileInput("Score.txt"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            lines++
            player.add(line.split(" ").toMutableList())
            line = br.readLine()
        }
        player.sortByDescending { it[1]}
        var user= ""
        var result=""
        var pos=""
        var num=0
        for (game in player){
            num++
            user += "${game[0]}\n"
            result += "${game[1]}\n"
            pos += "$num\n"
        }
//        binding.name.text= user
//        binding.result.text= result
//        binding.pos.text= pos
    }

}