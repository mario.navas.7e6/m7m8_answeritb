package com.example.trivialtest.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.trivialtest.R
import com.example.trivialtest.databinding.FragmentResultBinding
import com.example.trivialtest.viewmodel.QuestionsViewModel

class ResultFragment : Fragment() {

    private val viewModel: QuestionsViewModel by activityViewModels()
    private lateinit var binding: FragmentResultBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentResultBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.scoreText.text = viewModel.score.toString()


        binding.btnPlay.setOnClickListener {

            viewModel.submitScore(-viewModel.score)
            findNavController().navigate(R.id.action_resultFragment_to_questionsFragment)        }

        binding.btnMenu.setOnClickListener {

            viewModel.submitScore(-viewModel.score)
            findNavController().navigate(R.id.action_resultFragment_to_menuFragment)
        }

        binding.btnRanking.setOnClickListener{

        }
    }
}