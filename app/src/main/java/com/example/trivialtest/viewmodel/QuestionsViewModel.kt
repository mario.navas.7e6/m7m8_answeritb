package com.example.trivialtest.viewmodel


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.trivialtest.R
import com.example.trivialtest.model.Question

class QuestionsViewModel : ViewModel() {

    var questions = listOf<Question>(
        Question(
            "Cual es el primer largometraje de la historia? ",
            "four",
            "arts",
            "blanca nieves",
            listOf("mickey mouse",
                "blanca nieves",
                "mi vecino totoro",
                "Ninguna de las anteriores",),

            R.drawable.shrek_1
        ),

        Question("Quien compuso las Cuatro Estaciones? ",
            "four",
            "arts",
            "Vivaldi",
            listOf("Mozart",
            "Vivaldi",
            "Beethoven",
            "Bach"),
            R.drawable.cuatroestciones),

        Question("Michael Jackson murió en el 2008 ",
            "trueFalse",
            "arts",
            "false",
            listOf(),
            R.drawable.michaeljackson),
        Question(
            "En que años se acontenció la primera guerra munial (la Gran gurra)",
            "four",
            "history",
            "1914-1918",
            listOf("1913-1918",
            "1939-1945",
            "1914-1919",
            "1914-1918"),
            R.drawable.guerramundial
        ),
        Question(
            "Que personaje de la historia de España aparece en la fotografia?",
            "four",
            "history",
            "Lluis Companys",
            listOf("Lluis Companys",
            "Gil Robles",
            "Calvo Sotelo",
            "Alcala Zamora"),
            R.drawable.lluiscompanys
        ),
        Question("Aproximadamente, en el año 1800 d.C. comienza la Edad... ",
            "input",
            "history",
            "Conetemporanea",
            listOf(),
            R.drawable.revolucionfrancesa),

        Question("A que cientifico famoso se le cayo una manzana en la cabeza?",
            "four",
            "science",
            "Isaac Newton",
            listOf("Alexander Fleming",
            "Albert Einstein",
            "Isaac Newton",
            "Charles Darwin"),
            R.drawable.newton),

        Question("109 es un numero primo",
            "trueFalse",
            "science",
            "true",
            listOf(),
            R.drawable.skate),

        Question("Simbolo del Sodio en la tabla períodica",
            "input",
            "science",
            "",
            listOf(),
            R.drawable.taulaperiodica)
        ,

        Question("Que equpio tiene mas campeonatos de la NBA",
            "four",
            "sports",
            "Boston Celtics",
            listOf("Detroit Pistons",
            "Chicago Bulls",
            "Golden State Warriors",
            "Boston Celtics"),
            R.drawable.nba)
        ,

        Question("El SkateBoarding se considera una disciplina olímpica.",
            "trueFalse",
            "sports",
            "true",
            listOf(),
            R.drawable.skate)
        ,

        Question("Como se llama el objeto con ruedas de la imagen?",
            "input",
            "sports",
            "skate",
            listOf(),
            R.drawable.skate)
        ,

        Question("Cual de estos paises, no pertenece al continente oceánico (Oceanía)",
            "four",
            "geography",
            "Togo",
            listOf("Fiyi",
            "Togo",
            "Tonga",
            "Papua Nueva Guinea"),
            R.drawable.oceania)
        ,

        Question("España esta en los cinco paises con mayor población del continente Europeo",
            "trueFalse",
            "geography",
            "false",
            listOf(),
            R.drawable.espanya)
        ,

        Question("Capital de Austria",
            "input",
            "geography",
            "Viena",
            listOf(),
            R.drawable.austria)

    )
    init {
        shuffleQuestions()
    }

    var questionIndex = 0
    val currentQuestion = MutableLiveData(questions[questionIndex])
    var score = 0


    fun submitScore(newScore: Int) {
        score += newScore
    }

    fun getNextQuestion() {
       questionIndex += 1
        currentQuestion.value = questions[questionIndex]
    }

    fun isLastQuestion(): Boolean {
        return questionIndex == 5
    }

    fun shuffleQuestions() {
        questions = questions.shuffled()
    }
}